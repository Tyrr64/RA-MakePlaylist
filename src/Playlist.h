#ifndef __PLAYLIST_H__
#define __PLAYLIST_H__
#include "Incs.h"

class CPlaylistCreator
{
private:
	std::vector<std::pair<std::string, std::string>> m_vDirFiles; //Holds full file path and name w/o extension.
	
	const std::string m_szCurrentPath = fs::current_path().string() + "/";
	std::string m_szWorkingDirectory = "";

	bool m_bRecursive = false;
	std::size_t m_iDirCount = 0;

public:
	void GetFolderFiles(fs::path DirPath, bool IsRecursive = false);
	bool CreatePlaylist(const std::string_view PlaylistName);
	void GetMultiPath(const std::string_view Paths);

	void Debug();
};

#endif //__PLAYLIST_H__
