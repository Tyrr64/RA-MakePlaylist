#ifndef __INCS_H__
#define __INCS_H__

#include <iostream>
#include <string>
#include <vector>
#include <filesystem>
#include <fstream>
#include <string_view>
#include <algorithm>
#include <cstring>
#include <string.h>
#include <unistd.h>
#include <chrono>
#include <iomanip>

namespace fs = std::filesystem;

inline const std::string ColorRed(const std::string String)
{
	return "\033[1;31m" + String + "\033[0m\n";
}

template <typename _Mty>
inline const std::string ColorRed(const _Mty Number)
{
	return "\033[1;31m" + std::to_string(Number) + "\033[0m\n";
}


inline const std::string ColorGreen(const std::string String)
{
	return "\033[1;32m" + String + "\033[0m\n";
}

template <typename _Mty>
inline const std::string ColorGreen(const _Mty Number)
{
	return "\033[1;32m" + std::to_string(Number) + "\033[0m\n";
}

#endif //__INCS_H__
