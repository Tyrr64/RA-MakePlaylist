#include "Playlist.h"

void CPlaylistCreator::GetFolderFiles(fs::path DirPath, bool IsRecursive)
{
    m_bRecursive = IsRecursive;

    if(DirPath.string()[0] != '/') //if its a relative path...
        DirPath = (m_szCurrentPath + DirPath.string());

    if(m_bRecursive)
    {
        for(const auto& Entry : fs::recursive_directory_iterator(DirPath))
        {
            if(Entry.is_directory())
            {
                ++m_iDirCount;
                continue;
            }
            else if(Entry.is_regular_file())
            {
                m_vDirFiles.push_back({	Entry.path().c_str(),
                                        Entry.path().filename().replace_extension().filename().c_str() });
            }            
        }
    }
    else
    {
        for(const auto& Entry : fs::directory_iterator(DirPath))
        {
            m_vDirFiles.push_back({	Entry.path().c_str(),
                                    Entry.path().filename().replace_extension().filename().c_str() });
        }
    }

    //Sort by filename not path.
    std::sort(m_vDirFiles.begin(), m_vDirFiles.end(),
             [](const std::pair<std::string, std::string>& a,
                const std::pair<std::string, std::string>& b) 
               {
                    return a.second < b.second;
               } );
}

bool CPlaylistCreator::CreatePlaylist(const std::string_view PlaylistName)
{
    const std::string SavePath = m_szCurrentPath + PlaylistName.data();

    std::fstream PlaylistFile(SavePath, std::ios::out);
    if(PlaylistFile)
    {
        for(const auto& Entry : m_vDirFiles)
        {
            PlaylistFile    << Entry.first   << "\n"
                            << Entry.second  << "\n"
                            << "DETECT" 	 << "\n"
                            << "DETECT" 	 << "\n"
                            << "0|crc" 	     << "\n"
                            << PlaylistName	 << "\n";
        }
        PlaylistFile << "\n";

        if(m_bRecursive)
        {
            std::cout << "Playlist created with: \n";
            std::cout << "\t[" << ColorGreen(m_vDirFiles.size()) << "] files in <" << ColorRed(m_iDirCount) << "> directories.\n";
        }
        else
        {
            std::cout << "Playlist created with [" << ColorGreen(m_vDirFiles.size()) << "] items.\n";
        }
        return true;
    }
    else
    {
        printf("Can't create file <%s>, on path <%s>.\nMake sure you have writing permissions there.\n", PlaylistName.data(), m_szCurrentPath.data());
        return false;
    }

    return false;
}

void CPlaylistCreator::Debug()
{
    if(m_bRecursive)
        printf("\033[1;31m [%lu] Files scanned in [%lu] Directories. \033[0m\n", m_vDirFiles.size(), m_iDirCount);

    for(const auto& Entry : m_vDirFiles)
        printf("Name: %sPath: %s\n\n", ColorRed(Entry.second).c_str(), ColorGreen(Entry.first).c_str());

    printf("\033[1;31m [%lu] Files scanned. \033[0m\n", m_vDirFiles.size());
}
