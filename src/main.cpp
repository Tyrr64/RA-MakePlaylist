#include "Playlist.h"

int PrintHelp();

int main(int ArgsC, char** ArgsV)
{
	if(ArgsC == 1)
	{
		std::cout << "Use -h for help.\n";
	}
	else
	{
		bool IsRecurive = false, PrintOnly = false;
		std::string PlayPath, PlayName;

		for(int i = 1; i < ArgsC; i++)
		{
			if(ArgsV[i][0] == '-')
			{
				if(ArgsV[i][1] == 'h')
				{
					return PrintHelp();
				}
				else if(ArgsV[i][1] == 'r')
				{
					IsRecurive = true;
					continue;
				}
				else if(ArgsV[i][1] == 'd')
				{
					PrintOnly = true;
					continue;
				}
				else if(ArgsV[i][1] == 'p')
				{
					PlayPath = ArgsV[++i];
					continue;
				}
				else if(ArgsV[i][1] == 'n')
				{
					PlayName = ArgsV[++i];
					continue;
				}
				else
				{
					std::cout << "Unknown option: " << ArgsV[i] << "\nExiting...\n";
					return 1;
				}
			}
			else
			{
				std::cout << "Unknown option: " << ArgsV[i] << "\nExiting...\n";
				return 1;
			}
		}


		CPlaylistCreator pl;

		const auto start = std::chrono::high_resolution_clock::now();

		pl.GetFolderFiles(PlayPath, IsRecurive);
		
		if(PrintOnly)
		{
			pl.Debug();
		}
		else
		{
			pl.CreatePlaylist(PlayName);
		}

		const auto end = std::chrono::high_resolution_clock::now();
		std::cout << std::setprecision(3) << std::dec << "Execution Time: [\033[1;31m" << std::chrono::duration<double, std::milli>(end - start).count() << "\033[0m] ms\n";
	}

	return 0;
}

int PrintHelp()
{
	std::cout << "Tyrr64 CLI RetroArch Playlist Maker. Version 0.1\n";
	std::cout << "Usage: \n";
	std::cout << "\t-r Recursive scan.\n";
	std::cout << "\t-p Path to scan.\n";
	std::cout << "\t-n Playlist name (used for filename and id).\n\n";

	return 1;
}
