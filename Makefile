SRC_DIR = ./src

DEBUG = 1
CXX = clang++
TARGET_NAME = MakePlaylist

SRCS = main.cpp Playlist.cpp

WARNING_FLAGS = -Wall -Wextra -Wno-c++98-compat
DEFAULT_FLAGS = -std=c++17
OPT_FLAGS = -O3 -march=bdver1 -mtune=bdver1
DBG_FLAGS = -O0 -g

LD = $(CXX)
CXXFLAGS = $(DEFAULT_FLAGS) $(WARNING_FLAGS)

ifeq ($(DEBUG), 1)
	CXXFLAGS += $(DBG_FLAGS)
	LDFLAGS = -fuse-ld=lld -lstdc++fs
	BIN_DIR = ./bin/debug/
	OBJ_DIR = ./bin/debug/obj
else
	CXXFLAGS += $(OPT_FLAGS)
	LDFLAGS = -fuse-ld=lld -flto -O3 -s -lstdc++fs
	BIN_DIR = ./bin/release/
	OBJ_DIR = ./bin/release/obj
endif

$(OBJ_DIR)/%.o : $(SRC_DIR)/%.cpp
	$(CXX) $^ -o $@ -c $(CXXFLAGS)

TARGET_BIN = $(BIN_DIR)/$(TARGET_NAME)

all : make_dirs $(TARGET_BIN)

$(TARGET_BIN) : $(SRCS:%.cpp=$(OBJ_DIR)/%.o)
	$(LD) -o $@ $^ $(LDFLAGS)

clean:
	rm -f -r ./bin

make_dirs :
	mkdir -p $(OBJ_DIR)
	mkdir -p $(BIN_DIR)
